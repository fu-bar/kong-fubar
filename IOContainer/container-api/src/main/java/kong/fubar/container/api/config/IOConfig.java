package kong.fubar.container.api.config;

import java.lang.annotation.Annotation;
import java.util.List;

public class IOConfig {
    private Class<? extends Annotation> injectAnnotationClass;
    private List<String> injectionWhiteList;
    private List<String> injectionBlackList;
    private List<String> injectionPackagePrefixes;

    public Class<? extends Annotation> getInjectAnnotationClass() {
        return injectAnnotationClass;
    }

    public List<String> getInjectionWhiteList() {
        return injectionWhiteList;
    }

    public List<String> getInjectionBlackList() {
        return injectionBlackList;
    }

    public List<String> getInjectionPackagePrefixes() {
        return List.copyOf(injectionPackagePrefixes);
    }
}
