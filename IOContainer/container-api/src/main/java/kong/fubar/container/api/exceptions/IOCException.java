package kong.fubar.container.api.exceptions;

public class IOCException extends RuntimeException {
    public IOCException(String errorMessage) {
        super(errorMessage);
    }

    public IOCException(String errorMessage, Throwable cause) {
        super(errorMessage, cause);
    }
}
