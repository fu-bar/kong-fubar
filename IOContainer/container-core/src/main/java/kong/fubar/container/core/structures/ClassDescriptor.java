package kong.fubar.container.core.structures;

import com.google.common.base.Objects;
import kong.fubar.container.api.exceptions.IOCException;
import kong.fubar.container.api.interfaces.Inject;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.util.*;
import java.util.stream.Collectors;

public class ClassDescriptor {
    private final Class<?> clazz;
    private final List<Constructor<?>> sortedConstructors;
    private final Annotation[] annotations;
    private final Class<?>[] interfaces;
    private final boolean isSingleton;

    public ClassDescriptor(Class<?> clazz) {
        // Currently, only Inject.class is supported
        if (!clazz.isAnnotationPresent(Inject.class)) {
            String errorMessage = String.format("Class %s is not annotated with @Inject", clazz.getCanonicalName());
            throw new IOCException(errorMessage);
        }
        this.clazz = clazz;
        this.sortedConstructors = Arrays.stream(clazz.getConstructors()).sorted(Comparator.comparingInt(Constructor::getParameterCount)).collect(Collectors.toList());
        Collections.reverse(this.sortedConstructors);
        this.annotations = clazz.getAnnotations();
        this.interfaces = clazz.getInterfaces();

        Inject injectAnnotation = clazz.getAnnotation(Inject.class);
        this.isSingleton = injectAnnotation.asSingleton();

    }

    public Class<?> getClazz() {
        return clazz;
    }

    public List<Constructor<?>> getSortedConstructors() {
        return List.copyOf(sortedConstructors);
    }

    public Annotation[] getAnnotations() {
        return annotations.clone();
    }

    public List<Class<?>> getInterfaces() {
        return Arrays.asList(interfaces);
    }

    public boolean isConstructableFrom(Set<Class<?>> resolvedClasses) {
        for (Constructor<?> candidate : this.sortedConstructors) {
            if (Arrays.stream(candidate.getParameterTypes()).allMatch(resolvedClasses::contains)) {
                return true;
            }
        }
        return false;
    }

    public boolean isSingleton() {
        return isSingleton;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        ClassDescriptor that = (ClassDescriptor) other;
        return Objects.equal(clazz, that.clazz);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(clazz);
    }
}
