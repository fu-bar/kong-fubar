package kong.fubar.container.core;

import kong.fubar.container.api.config.IOConfig;
import kong.fubar.container.core.structures.ClassDescriptor;
import kong.fubar.container.core.structures.ClassInspector;
import kong.fubar.container.core.structures.ClassPathInspector;
import kong.fubar.container.core.utilities.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

public class IOContainer {
    private ClassInspector classInspector;

    public IOContainer(String configFilePath) {
        Logger log = LogManager.getLogger(IOContainer.class);
        File configFile = new File(configFilePath);
        if (configFile.isDirectory() || !configFile.exists()) {
            log.error("[ABORTING] - Configuration file does not exist: {}", configFilePath);
            System.exit(-1);
        }
        try {
            IOConfig config = Utils.OBJECT_MAPPER.readValue(new File(configFilePath), IOConfig.class);
            ClassPathInspector classPathInspector = new ClassPathInspector(config);
            Set<Class<?>> injectedClasses = classPathInspector.getInjectedClasses();
            log.debug("Discovered {} classes marked with @{}", injectedClasses.size(), config.getInjectAnnotationClass().getSimpleName());
            this.classInspector = new ClassInspector(injectedClasses);
            this.classInspector.inspect();
        } catch (IOException e) {
            log.error("[ABORTING] - Failed reading {}", configFilePath, e);
            System.exit(-1);
        }
    }

    public boolean isReady() {
        return this.classInspector.isReady();
    }

    public Set<Class<?>> getResolvedClasses() {
        return this.classInspector.getResolvedClasses();
    }

    public Set<Class<?>> getUnresolvedClasses() {
        return this.classInspector.getUnresolvedDescriptors().stream()
                .map(ClassDescriptor::getClazz)
                .collect(Collectors.toSet());
    }

    public <T> T getInstance(Class<? extends T> classOrInterface) {
        return this.classInspector.getInstance(classOrInterface);
    }
}
