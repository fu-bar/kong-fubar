package kong.fubar.container.core.structures;

import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import kong.fubar.container.api.exceptions.IOCException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Collectors;

public class ClassInspector {
    private final Set<Class<?>> resolvedClasses = new HashSet<>();
    private final Set<ClassDescriptor> unresolvedDescriptors = new HashSet<>();
    private final Logger log = LogManager.getLogger(ClassInspector.class);
    private final Multimap<Class<?>, ClassDescriptor> classToDescriptors = MultimapBuilder.hashKeys().hashSetValues().build();
    private final Map<Class<?>, Object> singletons = new HashMap<>();


    public ClassInspector(Set<Class<?>> injectedClasses) {
        for (Class<?> clazz : injectedClasses) {
            ClassDescriptor classDescriptor = new ClassDescriptor(clazz);
            classToDescriptors.put(clazz, classDescriptor);
            for (var implementedInterface : clazz.getInterfaces()) {
                classToDescriptors.put(implementedInterface, classDescriptor);
            }
        }
    }

    /**
     * Inspect classes provided in the constructor
     */
    public void inspect() {
        unresolvedDescriptors.addAll(classToDescriptors.values());
        boolean madeProgress = true;
        while (madeProgress) {
            madeProgress = false;
            for (var descriptor : List.copyOf(unresolvedDescriptors)) {
                if (descriptor.isConstructableFrom(resolvedClasses)) {
                    this.resolvedClasses.add(descriptor.getClazz());
                    this.resolvedClasses.addAll(descriptor.getInterfaces());
                    this.unresolvedDescriptors.remove(descriptor);
                    log.debug("Successfully resolved {}", descriptor.getClazz().getName());
                    madeProgress = true;
                }
            }
        }
        if (!this.unresolvedDescriptors.isEmpty()) {
            log.warn("The following classes were not resolved: {}", this.unresolvedDescriptors.stream().map(descriptor -> descriptor.getClazz().getName()).collect(Collectors.toList()));
        }
    }

    public List<ClassDescriptor> getUnresolvedDescriptors() {
        return List.copyOf(unresolvedDescriptors);
    }

    public Set<Class<?>> getResolvedClasses() {
        return Set.copyOf(this.resolvedClasses);
    }

    public boolean isReady() {
        return this.unresolvedDescriptors.isEmpty();
    }

    public <T> T getInstance(Class<?> classOrInterface) {
        if (!this.classToDescriptors.containsKey(classOrInterface)) {
            throw new IOCException(String.format("%s is not resolved among %s", classOrInterface.getName(), this.resolvedClasses));
        }
        Collection<ClassDescriptor> classDescriptors = this.classToDescriptors.get(classOrInterface);
        if (classDescriptors.size() > 1) {
            throw new IOCException(String.format("%d candidates for %s - %s", classDescriptors.size(), classOrInterface.getName(), classDescriptors));
        }
        ClassDescriptor classDescriptor = classDescriptors.iterator().next();
        if (classDescriptor.isSingleton() && this.singletons.containsKey(classOrInterface)) {
            return (T) this.singletons.get(classOrInterface);
        }
        List<Constructor<?>> sortedConstructors = classDescriptor.getSortedConstructors();
        for (var constructor : sortedConstructors) {
            Class<?>[] parameterTypes = constructor.getParameterTypes();
            if (parameterTypes.length == 0) {
                return constructDefault(classOrInterface, classDescriptor, constructor);
            }
            if (Arrays.stream(parameterTypes).anyMatch(aClass -> !this.resolvedClasses.contains(aClass))) {
                continue;
            }
            return constructParametrised(classOrInterface, classDescriptor, constructor, parameterTypes);
        }
        String errorMessage = String.format("[BUG?] %s is marked as resolved but failed to find a suitable constructor for it", classOrInterface);
        throw new IOCException(errorMessage);
    }

    @NotNull
    private <T> T constructParametrised(Class<?> classOrInterface, ClassDescriptor classDescriptor, Constructor<?> constructor, Class<?>[] parameterTypes) {
        Object[] paramInstances = new Object[parameterTypes.length];
        for (int i = 0; i < parameterTypes.length; i++) {
            paramInstances[i] = getInstance(parameterTypes[i]);
        }
        try {
            log.debug("Instantiating {} with a constructor of params: {}", classDescriptor.getClazz().getName(),
                    Arrays.stream(paramInstances).map(param -> param.getClass().getName()).collect(Collectors.toList())
            );
            T instance = (T) constructor.newInstance(paramInstances);
            if (classDescriptor.isSingleton()) {
                this.singletons.put(classOrInterface, instance);
            }
            return instance;
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            String errorMessage = String.format("Unexpected failure calling constructor of %s with params of %s", classDescriptor.getClazz().getName(), List.of(paramInstances));
            throw new IOCException(errorMessage, e);
        }
    }

    @NotNull
    private <T> T constructDefault(Class<?> classOrInterface, ClassDescriptor classDescriptor, Constructor<?> constructor) {
        try {
            log.debug("Instantiating {} with a default constructor", classDescriptor.getClazz().getName());
            T instance = (T) constructor.newInstance();
            if (classDescriptor.isSingleton()) {
                this.singletons.put(classOrInterface, instance);
                for (var implementedInterface : instance.getClass().getInterfaces()) {
                    this.singletons.put(implementedInterface, instance);
                }
            }
            return instance;
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            String errorMessage = String.format("Unexpected failure calling default constructor of %s", classDescriptor.getClazz().getName());
            throw new IOCException(errorMessage, e);
        }
    }
}
