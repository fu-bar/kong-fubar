package kong.fubar.container.core.structures;

import kong.fubar.container.api.config.IOConfig;
import org.reflections.Reflections;

import java.util.Set;

public class ClassPathInspector {
    private final Reflections reflections;
    private final IOConfig config;

    public ClassPathInspector(IOConfig config) {
        this.config = config;
        this.reflections = new Reflections(config.getInjectionPackagePrefixes());
    }

    public Set<Class<?>> getInjectedClasses() {
        return reflections.getTypesAnnotatedWith(config.getInjectAnnotationClass());
    }
}
