package kong.fubar.container.playground;


import kong.fubar.container.core.IOContainer;
import kong.fubar.container.playground.samples.Person;

public class App {
    public static void main(String[] args) {
        IOContainer ioContainer = new IOContainer(args[0]);
        Person instance = ioContainer.getInstance(Person.class);
    }
}
