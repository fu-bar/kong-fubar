package kong.fubar.container.playground.samples;


import kong.fubar.container.api.interfaces.Inject;

@Inject
public class Person {
    private final Fruit favoriteFruit;
    private final Cake favoriteCake;

    public Person(Fruit favoriteFruit, Cake favoriteCake) {
        this.favoriteFruit = favoriteFruit;
        this.favoriteCake = favoriteCake;
    }
}
