package kong.fubar.container.playground.samples;


import kong.fubar.container.api.interfaces.Inject;

@Inject
public class Cake {
    private final Fruit taste;

    public Cake(Fruit taste) {
        this.taste = taste;
    }
}
