package kong.fubar.rest.server.config;

import io.dropwizard.Configuration;

import java.util.List;
import java.util.Map;

public class Config extends Configuration {
    private Map<String, Map<String, Object>> pluginsConfig;
    private String pluginsPackagePrefix;
    private List<String> pluginsWhiteList;
    private List<String> pluginsBlackList;

    public Map<String, Map<String, Object>> getPluginsConfig() {
        return pluginsConfig;
    }

    public String getPluginsPackagePrefix() {
        return pluginsPackagePrefix;
    }

    public List<String> getPluginsWhiteList() {
        return pluginsWhiteList;
    }

    public List<String> getPluginsBlackList() {
        return pluginsBlackList;
    }
}
