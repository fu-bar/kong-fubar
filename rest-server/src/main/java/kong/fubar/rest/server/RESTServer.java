package kong.fubar.rest.server;

import com.codahale.metrics.health.HealthCheck;
import io.dropwizard.Application;
import io.dropwizard.setup.Environment;
import kong.fubar.rest.server.config.Config;
import kong.fubar.rest.server.plugins.base.OSFServerPlugin;
import kong.fubar.rest.server.utils.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class RESTServer extends Application<Config> {
    private static final Logger log = LogManager.getLogger(RESTServer.class);


    public static void main(String[] args) throws Exception {
        String configFilePath;
        if (args.length > 0) {
            configFilePath = args[0];
            log.debug("Configuration file is set to: {}", configFilePath);
        } else {
            configFilePath = "./config.yaml";
            log.warn("Configuration file was not supplied in command line, defaulting to: {}", configFilePath);
        }
        if (!Utils.fileExists(configFilePath)) {
            abort(String.format("Configuration file not found: %s", configFilePath), null);
        }
        Utils.printLogo();
        RESTServer RESTServer = new RESTServer();
        RESTServer.run("server", configFilePath);
    }

    public static void abort(String errorMessage, Throwable throwable) {
        if (throwable != null) {
            RESTServer.log.error("[Terminating] {}", errorMessage, throwable);
        } else {
            RESTServer.log.error("[Terminating] {}", errorMessage);
        }
        System.exit(1);
    }

    private boolean isPluginAllowed(String pluginName, List<String> pluginsWhiteList, List<String> pluginsBlackList) {
        if (!pluginsWhiteList.isEmpty() && !pluginsWhiteList.contains(pluginName)) {
            log.warn("[SKIPPING NON-WHITELISTED PLUGIN] {} is missing from the white-list in configurations {}", pluginName, pluginsWhiteList);
            return false;
        }
        if (!pluginsBlackList.isEmpty() && pluginsBlackList.contains(pluginName)) {
            log.warn("[SKIPPING BLACKLISTED PLUGIN] {} appears in the black-list in configurations {}", pluginName, pluginsBlackList);
            return false;
        }
        return true;
    }

    @Override
    public void run(Config configuration, Environment environment) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        environment.healthChecks().register("dummy", new HealthCheck() {
            @Override
            protected Result check() {
                return Result.healthy();
            }
        });
        String pluginsPackagePrefix = configuration.getPluginsPackagePrefix();
        Reflections reflections = new Reflections(pluginsPackagePrefix);
        var plugins = reflections.getSubTypesOf(OSFServerPlugin.class);
        log.debug("Discovered {} plugins", plugins.size());
        List<String> pluginsWhiteList = configuration.getPluginsWhiteList();
        List<String> pluginsBlackList = configuration.getPluginsBlackList();
        for (var plugin : plugins) {
            var constructor = plugin.getConstructor(Config.class);
            OSFServerPlugin osfServerPlugin = constructor.newInstance(configuration);
            if (!isPluginAllowed(plugin.getName(), pluginsWhiteList, pluginsBlackList)) {
                continue;
            }
            log.debug("Registering plugin: {}", osfServerPlugin.getName());
            environment.jersey().register(osfServerPlugin);
        }
    }
}
