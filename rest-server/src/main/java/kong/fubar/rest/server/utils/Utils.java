package kong.fubar.rest.server.utils;

import com.codahale.metrics.health.HealthCheck;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.setup.Environment;

import java.io.File;
import java.util.Map;

public class Utils {
    public static final ObjectMapper objectMapper = new ObjectMapper();

    private Utils() {
        // Static
    }

    public static boolean fileExists(String filePath) {
        File file = new File(filePath);
        return (file.exists() && !file.isDirectory());
    }


    public static void printLogo() {
        String logo = "██████╗ ███████╗███████╗████████╗    ███████╗███████╗██████╗ ██╗   ██╗███████╗██████╗ \n" +
                "██╔══██╗██╔════╝██╔════╝╚══██╔══╝    ██╔════╝██╔════╝██╔══██╗██║   ██║██╔════╝██╔══██╗\n" +
                "██████╔╝█████╗  ███████╗   ██║       ███████╗█████╗  ██████╔╝██║   ██║█████╗  ██████╔╝\n" +
                "██╔══██╗██╔══╝  ╚════██║   ██║       ╚════██║██╔══╝  ██╔══██╗╚██╗ ██╔╝██╔══╝  ██╔══██╗\n" +
                "██║  ██║███████╗███████║   ██║       ███████║███████╗██║  ██║ ╚████╔╝ ███████╗██║  ██║\n" +
                "╚═╝  ╚═╝╚══════╝╚══════╝   ╚═╝       ╚══════╝╚══════╝╚═╝  ╚═╝  ╚═══╝  ╚══════╝╚═╝  ╚═╝";
        System.out.println(logo);
    }

    public static void registerDummyHealthCheck(Environment environment) {
        environment.healthChecks().register("dummy", new HealthCheck() {
            @Override
            protected Result check() {
                return Result.healthy();
            }
        });
    }

    public static Map<String, String> buildMessage(String message) {
        return Map.of("message", message);
    }
}
