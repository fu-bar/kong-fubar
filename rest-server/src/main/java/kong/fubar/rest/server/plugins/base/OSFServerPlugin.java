package kong.fubar.rest.server.plugins.base;

import kong.fubar.rest.server.config.Config;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;


public abstract class OSFServerPlugin {
    protected final Logger log;
    protected Config configuration;

    public OSFServerPlugin(Config configuration) {
        this.log = LogManager.getLogger(this.getName());
        this.configuration = configuration;
    }

    public String getName() {
        return this.getClass().getSimpleName();
    }

    protected <T> T getConfigValue(String key) {
        if (this.configuration == null) {
            log.error("configuration is uninitialized (it is null)");
            return null;
        }
        if (!this.configuration.getPluginsConfig().containsKey(this.getName())) {
            log.error("Configuration does not have entries for {}", this.getName());
            return null;
        }
        Map<String, Object> keyValue = this.configuration.getPluginsConfig().get(this.getName());
        if (!keyValue.containsKey(key)) {
            log.error("Key {} not found under {} section in configuration", key, this.getName());
            return null;
        }
        return (T) keyValue.get(key);
    }
}
