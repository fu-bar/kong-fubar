package kong.fubar.rest.server.plugins;

import kong.fubar.rest.server.config.Config;
import kong.fubar.rest.server.plugins.base.OSFServerPlugin;
import kong.fubar.rest.server.utils.Utils;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Locale;

@Path("/config")
@Produces(MediaType.APPLICATION_JSON)
public class ConfigPlugin extends OSFServerPlugin {
    private final String contentFolder;
    private String state;
    private String configContent;

    public ConfigPlugin(Config configuration) {
        super(configuration);
        this.contentFolder = getConfigValue("contentFolder");
        if (!Files.exists(new File(this.contentFolder).toPath())) {
            log.fatal("Configured content-folder does not exist: {}", this.contentFolder);
            System.exit(-1);
        }
    }

    @GET
    @Path("/update")
    public Response update() {
        if (this.state == null) {
            return Response.serverError().entity(Utils.buildMessage("No state is set")).build();
        }
        return Response.ok(this.configContent).build();
    }

    @POST
    @Path("/setState")
    public Response setState(@QueryParam("state") String state) throws IOException {
        if (state.equals(this.state)) {
            log.warn("Current state is already {} which is the same as requested to update", this.state);
            return Response.ok().build();
        }
        var configFilePath = Paths.get(this.contentFolder, state.toLowerCase(Locale.ROOT) + ".json");
        if (!Files.exists(configFilePath)) {
            return Response.serverError().entity(Utils.buildMessage("No suitable configuration file found: " + configFilePath.toAbsolutePath())).build();
        }
        log.debug("Changing state to {}", state);
        this.state = state;
        log.debug("Reading configuration content from: {}", configFilePath.toAbsolutePath());
        this.configContent = Files.readString(configFilePath);
        return Response.ok().build();
    }
}
