package kong.fubar.bus.proxies.rest;

import kong.fubar.bus.exceptions.BusException;
import kong.fubar.bus.structures.Broker;
import kong.fubar.bus.structures.ClientProxy;
import kong.fubar.bus.structures.Message;
import kong.fubar.bus.utilities.Utils;
import okhttp3.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class RestClientProxy extends ClientProxy {
    private static final OkHttpClient CLIENT = new OkHttpClient();
    private final Logger log = LogManager.getLogger(RestClientProxy.class);
    private final String postAddress;

    public RestClientProxy(Broker broker, String postAddress) {
        super(broker);
        this.postAddress = postAddress;
    }

    @Override
    protected boolean accept(Message message) {
        if (this.postAddress == null) {
            throw new BusException("Can't deliver a message because listener post-address is not set");
        }

        String json = Utils.toJson(message);

        RequestBody requestBody = RequestBody.create(json, MediaType.parse("application/json"));

        Request request = new Request.Builder()
                .url(postAddress)
                .post(requestBody)
                .build();

        Call call = CLIENT.newCall(request);
        try {
            Response response = call.execute();
            return response.isSuccessful();
        } catch (IOException e) {
            log.error("Unexpected error while posting to client", e);
            return false;
        }
    }

    @Override
    protected boolean isAlive() {
        return true;
    }
}
