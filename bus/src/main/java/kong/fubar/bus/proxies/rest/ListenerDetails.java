package kong.fubar.bus.proxies.rest;

import com.google.common.base.Objects;

public class ListenerDetails {
    private final String postAddress;

    public ListenerDetails(String postAddress) {
        this.postAddress = postAddress;
    }

    public String getPostAddress() {
        return postAddress;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ListenerDetails that = (ListenerDetails) o;
        return Objects.equal(postAddress, that.postAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(postAddress);
    }
}
