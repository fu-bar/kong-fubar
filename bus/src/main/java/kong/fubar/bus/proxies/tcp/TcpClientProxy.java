package kong.fubar.bus.proxies.tcp;

import kong.fubar.bus.exceptions.BusException;
import kong.fubar.bus.structures.Broker;
import kong.fubar.bus.structures.ClientProxy;
import kong.fubar.bus.structures.Message;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class TcpClientProxy extends ClientProxy {
    private final InputStream inputStream;
    private final OutputStream outputStream;
    private final Socket clientSocket;

    protected TcpClientProxy(Broker broker, Socket clientSocket) {
        super(broker);
        try {
            this.clientSocket = clientSocket;
            this.inputStream = clientSocket.getInputStream();
            this.outputStream = clientSocket.getOutputStream();
        } catch (IOException e) {
            throw new BusException("Failed initializing streams from socket", e);
        }
    }

    @Override
    protected boolean accept(Message message) {
        return false;
    }

    @Override
    protected boolean isAlive() {
        return !this.clientSocket.isClosed();
    }
}
