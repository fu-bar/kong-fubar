package kong.fubar.bus.configuration;

import io.dropwizard.Configuration;

public class Config extends Configuration {
    private int multiClientTcpPort;

    public int getMultiClientTcpPort() {
        return multiClientTcpPort;
    }

    public void setMultiClientTcpPort(int multiClientTcpPort) {
        this.multiClientTcpPort = multiClientTcpPort;
    }
}
