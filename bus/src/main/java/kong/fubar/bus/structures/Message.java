package kong.fubar.bus.structures;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Map;

public class Message implements Serializable {
    private final String topic;
    private final byte[] payload;
    private final String sender;
    private final Map<String, Object> metadata;

    @JsonCreator
    public Message(@JsonProperty("topic") String topic,
                   @JsonProperty("payload") byte[] payload,
                   @JsonProperty("sender") String sender,
                   @JsonProperty("metadata") Map<String, Object> metadata) {
        this.topic = topic;
        this.payload = payload;
        this.sender = sender;
        this.metadata = Map.copyOf(metadata);
    }

    public String getTopic() {
        return topic;
    }

    public byte[] getPayload() {
        return Arrays.copyOf(payload, payload.length);
    }

    public String getSender() {
        return sender;
    }

    public Map<String, Object> getMetadata() {
        return Map.copyOf(metadata);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return Objects.equal(topic, message.topic) && Objects.equal(payload, message.payload) && Objects.equal(sender, message.sender) && Objects.equal(metadata, message.metadata);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(topic, payload, sender, metadata);
    }
}
