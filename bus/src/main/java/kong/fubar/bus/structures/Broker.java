package kong.fubar.bus.structures;

import kong.fubar.bus.multithreading.Task;
import kong.fubar.bus.multithreading.TaskExecutor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.Set;

public class Broker {
    private final TaskExecutor taskExecutor = new TaskExecutor();
    private final Set<ClientProxy> clientProxies = new HashSet<>();
    private final Logger log = LogManager.getLogger(Broker.class);

    public int publish(Message message, ClientProxy sender) {
        String topic = message.getTopic();

        synchronized (this) {
            int numberOfRecipients = 0;
            for (ClientProxy clientProxy : clientProxies) {
                if (clientProxy != sender && clientProxy.isSubscribedTo(topic)) {
                    taskExecutor.submit(new Task("Message Delivery", Duration.of(100, ChronoUnit.MILLIS)) {
                        @Override
                        protected void execute() {
                            clientProxy.accept(message);
                        }
                    });
                    numberOfRecipients++;
                }
            }
            return numberOfRecipients;
        }
    }

    public void update(ClientProxy clientProxy) {
        this.clientProxies.remove(clientProxy);
        this.clientProxies.add(clientProxy);
    }
}
