package kong.fubar.bus.structures;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashSet;
import java.util.Set;

public abstract class ClientProxy {
    protected final Broker broker;
    private final Logger log = LogManager.getLogger(this.getClass());
    private final Set<String> subscribedTopics = new HashSet<>();

    protected ClientProxy(Broker broker) {
        this.broker = broker;
    }

    protected void publish(Message message) {
        this.broker.publish(message, this);
    }

    protected abstract boolean accept(Message message);

    protected abstract boolean isAlive();

    public void subscribe(String topic) {
        this.subscribedTopics.add(topic);
    }

    public void unsubscribe(String topic) {
        this.subscribedTopics.remove(topic);
    }

    public boolean isSubscribedTo(String topic) {
        return this.subscribedTopics.contains(topic);
    }
}
