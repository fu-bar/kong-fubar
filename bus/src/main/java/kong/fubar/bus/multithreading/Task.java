package kong.fubar.bus.multithreading;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Duration;
import java.time.Instant;
import java.util.function.Consumer;

public abstract class Task {
    private final String taskName;
    private final Duration timeoutAfter;
    private final Logger log = LogManager.getLogger(Task.class);
    private Duration executionDuration;
    private Instant latestValidFinishTime;
    private boolean isTimedOut = false;
    private Consumer<Throwable> errorHandler;

    protected Task(String taskName) {
        this(taskName, Duration.ofDays(Long.MAX_VALUE), null);
    }

    protected Task(String taskName, Duration timeoutAfter) {
        this(taskName, timeoutAfter, null);
    }

    protected Task(String taskName, Duration timeoutAfter, Consumer<Throwable> errorHandler) {
        this.taskName = taskName;
        this.timeoutAfter = timeoutAfter;
        this.errorHandler = errorHandler;
    }

    public final boolean isTimedOut() {
        if (latestValidFinishTime == null) {
            return false;
        }
        return this.isTimedOut || Instant.now().isAfter(latestValidFinishTime);
    }

    public final void start() {
        Instant startTime = Instant.now();
        this.latestValidFinishTime = startTime.plus(this.timeoutAfter);
        try {
            execute();
            this.executionDuration = Duration.between(startTime, Instant.now());
        } catch (InterruptedException e) {
            this.isTimedOut = true;
            log.error("Execution of \"{}\" interrupted before task finished", taskName);
            Thread.currentThread().interrupt();
        } catch (Exception exception) {
            if (this.errorHandler != null) {
                this.errorHandler.accept(exception);
            } else {
                throw exception;
            }
        }
    }

    public final String getTaskName() {
        return taskName;
    }

    public final Duration getExecutionDuration() {
        return this.executionDuration;
    }

    public final void setErrorHandler(Consumer<Throwable> errorHandler) {
        this.errorHandler = errorHandler;
    }

    protected abstract void execute() throws InterruptedException;
}
