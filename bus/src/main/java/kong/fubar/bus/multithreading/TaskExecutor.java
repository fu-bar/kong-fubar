package kong.fubar.bus.multithreading;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class TaskExecutor {
    private final BlockingQueue<Task> tasks = new LinkedBlockingQueue<>();
    private final Timer inspectionTimer;
    private final List<TaskConsumer> taskConsumers = new ArrayList<>();
    private final Logger log = LogManager.getLogger(TaskExecutor.class);

    public TaskExecutor(int numberOfConsumers, int timeoutInspectionPeriodMilli) {

        log.debug("Starting timeout-executor with {} consumers", numberOfConsumers);
        for (int i = 0; i < numberOfConsumers; i++) {
            TaskConsumer taskConsumer = new TaskConsumer(this.tasks);
            taskConsumer.start();
            taskConsumers.add(taskConsumer);
        }

        log.debug("Starting inspection timer with interval of {} milliseconds", timeoutInspectionPeriodMilli);
        this.inspectionTimer = new Timer("Inspector Timer");
        TimerTask inspectionTask = new TimerTask() {
            @Override
            public void run() {
                for (TaskConsumer taskConsumer : taskConsumers) {
                    taskConsumer.validateTimeLimit();
                }
            }
        };
        inspectionTimer.scheduleAtFixedRate(inspectionTask, 0, timeoutInspectionPeriodMilli);
    }

    public TaskExecutor() {
        this(1, 50);
    }

    public void submit(Task task) {
        this.tasks.add(task);
    }

    public void stop() {
        log.debug("Stopping");
        for (TaskConsumer taskConsumer : taskConsumers) {
            taskConsumer.requireToStop();
        }
        this.inspectionTimer.cancel();
    }
}
