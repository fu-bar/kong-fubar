package kong.fubar.bus.multithreading;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.BlockingQueue;

public class TaskConsumer extends Thread {
    private final BlockingQueue<Task> tasks;
    private final Logger log = LogManager.getLogger(TaskConsumer.class);
    private Task currentTask = null;
    private boolean isRequiredToStop = false;

    public TaskConsumer(BlockingQueue<Task> tasks) {
        this.tasks = tasks;
        this.setDaemon(true);
    }

    public void requireToStop() {
        this.isRequiredToStop = true;
        this.interrupt();
    }

    public void validateTimeLimit() {
        if (this.currentTask == null) {
            return;
        }
        if (this.currentTask.isTimedOut()) {
            this.interrupt();
        }
    }

    @Override
    public void run() {
        super.run();
        log.debug("Starting tasks consumption");
        while (!isRequiredToStop) {
            try {
                currentTask = tasks.take();
                this.setName(currentTask.getTaskName());
                currentTask.start();
                this.currentTask = null;
            } catch (InterruptedException interruptedException) {
                if (this.currentTask != null) {
                    log.warn("Task {} has timed out", this.currentTask.getTaskName());
                }
            } catch (Throwable throwable) {
                log.error("Unexpected throw in executed task", throwable);
            }
        }
    }
}
