package kong.fubar.bus.utilities;

import com.codahale.metrics.health.HealthCheck;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.dropwizard.setup.Environment;
import kong.fubar.bus.exceptions.BusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.nio.ByteBuffer;
import java.util.Map;

public class Utils {
    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final Logger log = LogManager.getLogger(Utils.class);
    private static final String MIN_TIMESTAMP_KEY = "minTimestamp";
    private static final String MAX_TIMESTAMP_KEY = "maxTimestamp";

    static {
        OBJECT_MAPPER.registerModule(new JavaTimeModule());
        OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        OBJECT_MAPPER.activateDefaultTyping(OBJECT_MAPPER.getPolymorphicTypeValidator());
    }

    private Utils() {
        // Static
    }

    public static boolean fileExists(String filePath) {
        File file = new File(filePath);
        return (file.exists() && !file.isDirectory());
    }


    public static void printLogo() {
        String logo = "██████╗     ██╗   ██╗    ███████╗\n" +
                "██╔══██╗    ██║   ██║    ██╔════╝\n" +
                "██████╔╝    ██║   ██║    ███████╗\n" +
                "██╔══██╗    ██║   ██║    ╚════██║\n" +
                "██████╔╝    ╚██████╔╝    ███████║\n" +
                "╚═════╝      ╚═════╝     ╚══════╝";
        System.out.println(logo);
    }

    public static void registerDummyHealthCheck(Environment environment) {
        environment.healthChecks().register("dummy", new HealthCheck() {
            @Override
            protected Result check() {
                return Result.healthy();
            }
        });
    }

    public static void abort(String errorMessage, Throwable throwable) {
        if (throwable != null) {
            log.error("[Terminating] {}", errorMessage, throwable);
        } else {
            log.error("[Terminating] {}", errorMessage);
        }
        System.exit(1);
    }

    public static int bytesToInteger(byte[] bytes) {
        ByteBuffer contentSize = ByteBuffer.wrap(bytes);
        return contentSize.getInt();
    }


    public static String toJson(Object object) {
        try {
            return OBJECT_MAPPER.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            log.error("Failed serializing {} into a JSON string", object.getClass().getCanonicalName(), e);
            throw new BusException("Failed serializing into JSON", e);
        }
    }

    public static Map<String, Object> fromJson(String json) {
        try {
            return OBJECT_MAPPER.readValue(json, new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            throw new BusException("Failed serializing into JSON", e);
        }
    }
}
