package kong.fubar.bus.exceptions;

public class LogicException extends BusException {
    public LogicException(String errorMessage) {
        super(errorMessage);
    }

    public LogicException(String errorMessage, Throwable cause) {
        super(errorMessage, cause);
    }
}
