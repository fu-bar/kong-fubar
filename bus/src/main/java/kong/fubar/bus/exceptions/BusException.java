package kong.fubar.bus.exceptions;

public class BusException extends RuntimeException {
    public BusException(String errorMessage) {
        super(errorMessage);
    }

    public BusException(String errorMessage, Throwable cause) {
        super(errorMessage, cause);
    }
}
