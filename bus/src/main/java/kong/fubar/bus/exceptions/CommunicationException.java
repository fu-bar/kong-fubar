package kong.fubar.bus.exceptions;

public class CommunicationException extends BusException {
    public CommunicationException(String errorMessage) {
        super(errorMessage);
    }

    public CommunicationException(String errorMessage, Throwable cause) {
        super(errorMessage, cause);
    }
}
