package kong.fubar.bus.communication.tcp;

import kong.fubar.bus.exceptions.BusException;
import kong.fubar.bus.exceptions.ProtocolException;
import kong.fubar.bus.structures.Message;
import kong.fubar.bus.utilities.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.function.Consumer;


public class Protocol {
    public static final byte SUCCESS_MAGIC = 0;
    public static final byte ERROR_MAGIC = 1;
    public static final byte REGISTRATION_MAGIC = 2;
    public static final byte MESSAGE_MAGIC = 3;
    public static final byte GOODBYE_MAGIC = 4;
    public static final byte PERIOD_MAGIC = 5;
    private static final Logger log = LogManager.getLogger(Protocol.class);
    private static final int SIZE_OF_INT = 4;
    private static final String TOPIC_KEY = "topic";
    private static final String SENDER_KEY = "sender";

    private Protocol() {
        // Static
    }

    public static void readInput(InputStream inputStream, Consumer<TCPInput> tcpInputConsumer) {
        try {
            byte[] bytes = inputStream.readNBytes(1);
            byte opCode = bytes[0];
            switch (opCode) {
                case MESSAGE_MAGIC -> {
                    Message message = readMessage(inputStream);
                    TCPInput tcpInput = new TCPInput(opCode);
                    tcpInput.setMessage(message);
                    tcpInputConsumer.accept(tcpInput);
                }
                case REGISTRATION_MAGIC -> {
                    Map<String, Object> registrationData = Utils.fromJson(readMetadata(inputStream));
                    TCPInput tcpInput = new TCPInput(opCode);
                    tcpInput.setRegistrationData(registrationData);
                    tcpInputConsumer.accept(tcpInput);

                }
                case SUCCESS_MAGIC, GOODBYE_MAGIC -> {
                    validatePeriod(inputStream);
                    TCPInput tcpInput = new TCPInput(opCode);
                    tcpInputConsumer.accept(tcpInput);
                }
                case ERROR_MAGIC -> {
                    String errorMessage = new String(readSizedBytes(inputStream, true));
                    TCPInput tcpInput = new TCPInput(opCode);
                    tcpInput.setErrorMessage(errorMessage);
                    tcpInputConsumer.accept(tcpInput);
                }
                default -> throw new BusException("Unexpected opCode: " + opCode);
            }
        } catch (IOException e) {
            log.error("Failed reading from input-stream", e);
            throw new BusException("Failed reading from input-stream", e);
        }
    }

    private static Message readMessage(InputStream inputStream) {
        validateByte(inputStream, MESSAGE_MAGIC);
        Map<String, Object> metadata = Utils.fromJson(readMetadata(inputStream));
        String topic = (String) metadata.get(TOPIC_KEY);
        metadata.remove(TOPIC_KEY);
        String sender = (String) metadata.get(SENDER_KEY);
        metadata.remove(SENDER_KEY);
        byte[] payload = readPayload(inputStream);
        return new Message(topic, payload, sender, metadata);
    }

    public static void writeMessage(Message message, OutputStream outputStream) {
        try {
            outputStream.write(MESSAGE_MAGIC);
            Map<String, Object> metadata = Map.of(TOPIC_KEY, message.getTopic(), SENDER_KEY, message.getSender());
            sendSizedBytes(Utils.toJson(metadata).getBytes(StandardCharsets.UTF_8), outputStream, true);
            sendSizedBytes(message.getPayload(), outputStream, true);
        } catch (IOException e) {
            throw new BusException("Failed writing into output-stream", e);
        }
    }

    private static String readMetadata(InputStream inputStream) {
        byte[] metadataBytes = readSizedBytes(inputStream, true);
        return new String(metadataBytes);
    }

    private static byte[] readPayload(InputStream inputStream) {
        return readSizedBytes(inputStream, true);
    }


    private static byte[] readSizedBytes(InputStream inputStream, boolean expectPeriod) {
        try {
            byte[] payloadSizeBytes = inputStream.readNBytes(SIZE_OF_INT);
            int payloadSize = Utils.bytesToInteger(payloadSizeBytes);
            if (payloadSize != 0) {
                byte[] bytes = inputStream.readNBytes(payloadSize);
                if (expectPeriod) {
                    validatePeriod(inputStream);
                }
                return bytes;
            } else {
                return new byte[]{};
            }
        } catch (IOException e) {
            throw new BusException("Failed reading sized bytes");
        }
    }

    private static void sendSizedBytes(byte[] bytes, OutputStream outputStream, boolean addPeriod) {
        ByteBuffer sizeBuffer = ByteBuffer.allocate(SIZE_OF_INT);
        sizeBuffer.putInt(bytes.length);
        try {
            outputStream.write(sizeBuffer.array());
            outputStream.write(bytes);
            if (addPeriod) {
                outputStream.write(PERIOD_MAGIC);
            }
            outputStream.flush();
        } catch (IOException e) {
            log.error("Failed sending {} bytes.", bytes.length);
            throw new BusException("Failed sending sized byte-array", e);
        }
    }

    public static void disconnect(Socket clientSocket) {
        log.debug("Disconnecting from {}", clientSocket.getRemoteSocketAddress());
        OutputStream outputStream;
        try {
            outputStream = clientSocket.getOutputStream();
        } catch (IOException e) {
            throw new BusException("Failed getting an output stream from client socket", e);
        }
        try {
            outputStream.write(GOODBYE_MAGIC);
            ByteBuffer sizeBuffer = ByteBuffer.allocate(SIZE_OF_INT);
            sizeBuffer.putInt(0);
            outputStream.write(sizeBuffer.array());
            outputStream.write(PERIOD_MAGIC);
            outputStream.flush();
            clientSocket.close();
        } catch (IOException e) {
            log.error("Failed properly disconnecting from {}", clientSocket.getRemoteSocketAddress(), e);
            throw new BusException("Proper disconnection failed", e);
        }
    }

    public static void sendSuccess(OutputStream outputStream) {
        try {
            outputStream.write(new byte[]{SUCCESS_MAGIC, PERIOD_MAGIC});
            outputStream.flush();
        } catch (IOException e) {
            log.error("Failed reporting success to client", e);
        }
    }

    public static void sendError(String errorMessage, Socket clientSocket) {
        try {
            OutputStream outputStream = clientSocket.getOutputStream();
            outputStream.write(ERROR_MAGIC);
            byte[] messageBytes = errorMessage.getBytes(StandardCharsets.UTF_8);
            sendSizedBytes(messageBytes, outputStream, true);
            log.debug("Closing client socket");
            clientSocket.close();
        } catch (IOException e) {
            log.error("Failed delivering an error message to the client", e);
        }
    }

    public static void validatePeriod(InputStream inputStream) {
        validateByte(inputStream, PERIOD_MAGIC);
    }

    public static void validateByte(InputStream inputStream, byte requiredByte) {
        try {
            byte[] actualByte = inputStream.readNBytes(1);
            if (actualByte[0] != requiredByte) {
                log.error("Expected byte to be {} but it is {}", requiredByte, actualByte);
                throw new ProtocolException("Unexpected byte read");
            }
        } catch (IOException e) {
            log.error("Failed reading from input-stream", e);
            throw new BusException("Failed reading from input-stream", e);
        }
    }

    public static void validateSuccess(InputStream inputStream) throws IOException {
        byte[] bytes = inputStream.readNBytes(1);
        byte responseByte = bytes[0];
        switch (responseByte) {
            case SUCCESS_MAGIC:
                validatePeriod(inputStream);
                return;
            case ERROR_MAGIC:
                byte[] payloadSizeBytes = inputStream.readNBytes(4);
                int errorMessageSize = Utils.bytesToInteger(payloadSizeBytes);
                String errorMessage;
                if (errorMessageSize > 0) {
                    byte[] errorMessageBytes = inputStream.readNBytes(errorMessageSize);
                    errorMessage = new String(errorMessageBytes);
                    log.error(errorMessage);
                } else {
                    errorMessage = "Error message not provided (0 size)";
                }
                validatePeriod(inputStream);
                throw new ProtocolException(errorMessage);
            default:
                throw new ProtocolException(String.format("Unexpected read byte %d, expected %d or %d", responseByte, SUCCESS_MAGIC, ERROR_MAGIC));
        }
    }
}
