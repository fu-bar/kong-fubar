package kong.fubar.bus.communication.rest;

import kong.fubar.bus.configuration.Config;
import kong.fubar.bus.proxies.rest.RestClientProxy;
import kong.fubar.bus.structures.Broker;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class RequestHandler {
    private static final String MESSAGE_KEY = "message";
    private final Logger log = LogManager.getLogger(RequestHandler.class);
    private final Config config;
    private final Broker broker;
    private final RestClientProxy nonListeningClient;
    private final Map<String, RestClientProxy> postAddressToRestClientProxy = new HashMap<>();

    public RequestHandler(Config config, Broker broker) {
        this.config = config;
        this.broker = broker;
        this.nonListeningClient = new RestClientProxy(broker, null);
    }

    @POST
    @Path("/publish")
    public Response publish(PublishRequest publishRequest) {
        RestClientProxy clientProxy = postAddressToRestClientProxy.get(publishRequest.getCallbackPostAddress());
        if (clientProxy == null) {
            clientProxy = nonListeningClient;
        }
        int recipients = this.broker.publish(publishRequest.getMessage(), clientProxy);
        return Response.ok().entity(Map.of(MESSAGE_KEY, String.format("Delivered to %d recipients", recipients))).build();
    }

    @POST
    @Path("/subscribe")
    public Response subscribe(SubscribeRequest subscribeRequest) {
        String topic = subscribeRequest.getTopic();
        String postAddress = subscribeRequest.getPostAddress();
        this.postAddressToRestClientProxy.computeIfAbsent(postAddress, key -> new RestClientProxy(broker, postAddress));
        RestClientProxy clientProxy = this.postAddressToRestClientProxy.get(postAddress);
        clientProxy.subscribe(topic);
        this.broker.update(clientProxy);
        return Response.ok().build();
    }
}
