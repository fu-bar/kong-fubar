package kong.fubar.bus.communication.rest;

public class SubscribeRequest {
    private String topic;
    private String postAddress;

    public String getTopic() {
        return topic;
    }

    public String getPostAddress() {
        return postAddress;
    }
}
