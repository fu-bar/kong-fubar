package kong.fubar.bus.communication.rest;

import io.dropwizard.Application;
import io.dropwizard.setup.Environment;
import kong.fubar.bus.configuration.Config;
import kong.fubar.bus.structures.Broker;
import kong.fubar.bus.utilities.Utils;

public class RESTServer extends Application<Config> {
    private final Broker broker;
    private Config configuration;

    public RESTServer() {
        this.broker = new Broker();
    }

    @Override
    public void run(Config configuration, Environment environment) {
        this.configuration = configuration;
        Utils.registerDummyHealthCheck(environment);
        RequestHandler requestHandler = new RequestHandler(configuration, broker);
        environment.jersey().register(requestHandler);
    }

    public Config getConfiguration() {
        return configuration;
    }
}
