package kong.fubar.bus.communication.rest;

import kong.fubar.bus.structures.Message;

public class PublishRequest {
    private Message message;
    private String callbackPostAddress;

    public Message getMessage() {
        return message;
    }

    public String getCallbackPostAddress() {
        return callbackPostAddress;
    }
}
