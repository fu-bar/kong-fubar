package kong.fubar.bus.communication.tcp;

import kong.fubar.bus.structures.Message;

import java.time.Instant;
import java.util.Map;

public class TCPInput {
    private final byte opCode;
    private final Instant timestamp;
    private String errorMessage;
    private Message message;
    private Map<String, Object> metadata;
    private Map<String, Object> registrationData;

    public TCPInput(byte opCode) {
        this.timestamp = Instant.now();
        this.opCode = opCode;
    }

    public byte getOpCode() {
        return opCode;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public Map<String, Object> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, Object> metadata) {
        this.metadata = metadata;
    }

    public Map<String, Object> getRegistrationData() {
        return registrationData;
    }

    public void setRegistrationData(Map<String, Object> registrationData) {
        this.registrationData = registrationData;
    }
}
