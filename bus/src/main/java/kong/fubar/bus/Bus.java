package kong.fubar.bus;

import kong.fubar.bus.communication.rest.RESTServer;
import kong.fubar.bus.utilities.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Bus {
    private static final Logger log = LogManager.getLogger(Bus.class);

    public static void main(String[] args) throws Exception {
        String configFilePath;
        if (args.length > 0) {
            configFilePath = args[0];
            log.debug("Configuration file is set to: {}", configFilePath);
        } else {
            configFilePath = "config.json";
            log.warn("Configuration file was not supplied in command line, defaulting to: {}", configFilePath);
        }
        if (!Utils.fileExists(configFilePath)) {
            Utils.abort(String.format("Configuration file not found: %s", configFilePath), null);
        }

        Utils.printLogo();

        log.info("Configuration file: {}", configFilePath);
        RESTServer restServer = new RESTServer();
        restServer.run("server", configFilePath);
    }
}
